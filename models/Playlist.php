<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "playlist".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property integer $created_at
 * @property integer $order
 * @property string $provider
 * @property string $videoUrl
 */
class Playlist extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'playlist';
    }

    public function beforeSave($insert)
    {
        $this->created_at = time();
        $count = Playlist::find()->alias('p')->max('p.order');
        $count++;
        $this->order = $count;
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'],'trim'],
            [['title', 'url'], 'required','message'=>'Поле должно быть заполнено'],
            [['created_at', 'order'], 'safe'],
            [['title', 'url'], 'string', 'max' => 255],
            [['provider'], 'string', 'max' => 100],
            [['url'], 'unique'],
            ['url', 'match', 'pattern'=>'~(https:\/\/)?www\.youtube\.com\/watch\?v=(.{11})~','message'=>'Cсылка на ролик не корректна'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'url' => 'Ссылка на ролик',
            'created_at' => 'Время добавления',
            'order' => 'Order',
            'provider' => "Видеоплеер",
        ];
    }

    public function getVideoUrl()
    {
        preg_match('~www\.youtube\.com\/watch\?v=(.*)~', $this->url, $match);
        return "https://www.youtube.com/embed/" . $match[1];
    }
}
