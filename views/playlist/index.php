<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PlaylistSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Плейлист';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="playlist-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['view', 'id' => $model->id]);
                },

            ],
            'title',
            'url:url',
            'created_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(
                            Html::button('Редактировать', ['class' => 'btn btn-success']),
                            $url);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a(
                            Html::button('Удалить', ['class' => 'btn btn-danger']),
                            $url, [
                            'data' => [
                                'confirm' => 'Вы уверены что хотите удалить?',
                                'method' => 'post']
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
