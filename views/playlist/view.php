<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Playlist */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Плейлист', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
explode('=',parse_url($model->url,PHP_URL_QUERY ))[1];
?>
<div class="playlist-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
           'url:url',
            'provider'
        ],
    ]) ?>


    <iframe width="640" height="360" src="<?= $model->videoUrl?>" frameborder="0" allowfullscreen></iframe>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
